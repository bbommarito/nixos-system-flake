{ config, pkgs, ... }:

{
  fonts.fontconfig.enable = true;

  home = {
    homeDirectory = "/home/bbommarito";

    packages = with pkgs; [
      brave
      brillo
      discord
      maim
      neofetch
      redshift
      xclip
    ];

    stateVersion = "22.11";
    username = "bbommarito";
  };

  programs = { 
    alacritty.enable = true;

    home-manager.enable = true; 

    git = {
      enable = true;

      extraConfig = {
        pull = {
          rebase = true;
        };
      };

      signing = {
        key = "B7F3311E5DBD06DE";
        signByDefault = true;
      };

      userEmail = "brian@brianbommarito.xyz";
      userName = "Brian 'Burrito' Bommarito";
    };

    gpg = {
      enable = true;

      publicKeys = [
        {
          source = ./bbommarito.asc;
          trust = 5;
        }
      ];
    };

    lazygit.enable = true;

    neovim = {
      enable = true;

      plugins = with pkgs.vimPlugins; [
	vim-nix
      ];

      viAlias = true;
      vimAlias = true;
    };

    ssh.enable = true;

    starship = {
      enable = true;
      enableZshIntegration = true;
    };

    vscode = {
      enable = true;

      extensions = with pkgs.vscode-extensions; [
        bbenoist.nix
        ms-azuretools.vscode-docker
        ms-vscode-remote.remote-ssh
        vscodevim.vim
      ];
    };

    zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      enableSyntaxHighlighting = true;

      initExtra = ''
        gpg-connect-agent updatestartuptty /bye > /dev/null 
      ''; 

      sessionVariables = {
        KEYID = "B7F3311E5DBD06DE";
      };

      shellAliases = {
        dotfiles = "cd $HOME/src/nixos-flake";
        rebuild = "sudo nixos-rebuild switch --flake $HOME/src/nixos-flake/#";
      };
    };
  };

  services = {
    easyeffects = {
      enable = true;
      preset = "LoudnessEqualizer";
    };

    gpg-agent = {
      enable = true;
      enableScDaemon = true;
      enableSshSupport = true;
      enableZshIntegration = true;
    };

    picom = {
      backend = "glx";
      enable = true;
      vSync = true;
    };

    redshift = { 
      enable = true; 
      provider = "geoclue2";
    };

    xscreensaver = {
      enable = true;

      settings = {
        timeout = 60;
        lock = true;
        lockTimeout = "00:20:00";
        dpmsEnabled = true;
        dpmsStandby = "02:00:00";
        dpmsSuspend = "02:00:00";
        dpmsOff = "02:00:00";
        dpmsQuickOff = true;
        fade = true;
        unfade = false;
        fadeSeconds = 3;
        mode = "blank";
      };
    };
  };

  xdg.configFile."qtile" = {
    source = ./dots/config.py;
    target = "./qtile/config.py";
  };

  xdg.configFile."alacritty" = {
    source = ./dots/alacritty.yml;
    target = "./alacritty/alacritty.yml";
  };
}
