# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, vscode-server, ... }:

{
  imports = [       
    ./hardware-configuration.nix
    ./modules/xserver.nix
  ];

  console = {
    earlySetup = true; 
    enable = true;
    font = "${pkgs.terminus_font}/share/consolefonts/ter-132n.psf.gz";

    packages = with pkgs; [
      terminus_font
    ];
  };

  environment = {
    shells = with pkgs; [
      bashInteractive
      zsh
    ];

    systemPackages = with pkgs; [
      btop
      curl
      git
      lightlocker
      vim
      tmux
      wget
    ];
  };

  fonts.fonts = with pkgs; [
    (nerdfonts.override { 
      fonts = ["JetBrainsMono"];
    })
  ];

  hardware = {
    sane = {
      enable = true;

      extraBackends = with pkgs; [
        hplipWithPlugin
      ];
    };
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";

    extraLocaleSettings = {
      LC_TIME = "en_GB.UTF-8";
    };

    supportedLocales = [
      "en_GB.UTF-8/UTF-8"
      "en_US.UTF-8/UTF-8"
    ];
  };

  nix = {
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
  };

  nix = {
    settings.experimental-features = [ "nix-command" "flakes" ];
  };

  nixpkgs.config.allowUnfree = true;
  programs.dconf.enable = true;
  security.rtkit.enable = true;

  services = {
    geoclue2.enable = true;
    gnome.gnome-keyring.enable = true;
    openssh.enable = true;

    pipewire = {
      alsa = {
        enable = true;
        support32Bit = true;
      };

      enable = true;
      jack.enable = true;
      pulse.enable = true;
      wireplumber.enable = true;
    };

    printing = {
      drivers = with pkgs; [
        gutenprint
        hplipWithPlugin
      ];

      enable = true;
    };

    saned.enable = true;

    udev = {
      enable = true;

      extraRules = ''
        ACTION=="add", SUBSYSTEM=="backlight", RUN+="${pkgs.coreutils}/bin/chgrp video $sys$devpath/brightness", RUN+="/${pkgs.coreutils}/bin/chmod g+w $sys$devpath/brightness"
      '';
    };
  };

  time.timeZone = "America/Detroit";

  users.users.bbommarito = {
    createHome = true;

    extraGroups = [
      "audio"
      "lp"
      "networkmanager"
      "scanner"
      "video"
      "wheel"
    ];

    hashedPassword = "$6$rLkiiuVmXVxxHV9J$aE98MMduQi75V0jF8/15eP4LYvlUCM8uzLEeCyDJ3TBUBKzM4s6qYW8cqZX0y/FnrVrpRWSs.k2pCGB/6CDRp/";
    isNormalUser = true;
    shell = pkgs.zsh;
  };

  system.stateVersion = "22.11";
}

