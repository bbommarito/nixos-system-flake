{config, pkgs, ...}:
{
  services = {
    xserver = {
      dpi = 96;
      enable = true;

      displayManager = {
        autoLogin = {
          enable = true;
          user = "bbommarito";
        };

        defaultSession = "none+qtile";

        lightdm = {
          enable = true;

          greeters = {
            mini = {
              enable = true;
              user = "bbommarito";
            };
          };
        };
      };

      libinput = {
        touchpad = {
          clickMethod = "clickfinger";
          disableWhileTyping = true;
          middleEmulation = false;
          naturalScrolling = true;
          scrollMethod = "twofinger";
          tapping = true;
        };
        enable = true;
      };

      serverFlagsSection = ''
        Option "BlankTime" "0"
        Option "StandbyTime" "0"
        Option "SuspendTime" "0"
        Option "OffTime" "0"
      '';

      windowManager = {
        qtile = {
          enable = true;
        };
      };
    };
  };
}
